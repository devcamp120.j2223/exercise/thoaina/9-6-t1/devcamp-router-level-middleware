// import review model
const { mongoose } = require("mongoose");
const reviewModel = require("../models/reviewModel");

// get all reviews
const getAllReviewOfCourse = (req, res) => {
    reviewModel.find((error, data) => {
        if (error) {
            res.status(500).json({
                message: `Internal server error: ${error.message}`,
            });
        }
        else {
            res.status(200).json({
                ...data
            });
        }
    })
};

const getReviewById = (req, res) => {
    let id = req.params.reviewId;

    if (!mongoose.Types.ObjectId.isValid(id)) {
        res.status(400).json({
            message: "id is invalid!",
        })
    }
    else {
        reviewModel.findById(id, (error, data) => {
            if (error) {
                res.status(500).json({
                    message: `Internal server error: ${error.message}`,
                });
            }
            else {
                res.status(200).json({
                    data
                });
            }
        })
    }
};

const createReviewOfCourse = (req, res) => {
    // B1: thu thập dữ liệu
    let body = req.body;

    // B2: Kiểm tra dữ liệu
    if (!Number.isInteger(body.starts) || body.starts < 0) {
        res.status(400).json({
            message: "starts is invalid!",
        })
    }
    else {
        // B3: Thực hiện thao tác nghiệp vụ
        let review = {
            _id: mongoose.Types.ObjectId(),
            starts: body.starts,
        };
        reviewModel.create(review, (error, data) => {
            if (error) {
                res.status(500).json({
                    message: `Internal server error: ${error.message}`,
                });
            }
            else {
                res.status(201).json({
                    data
                });
            }
        });
    }
};

const updateReviewById = (req, res) => {
    let id = req.params.reviewId;
    let body = req.body;

    if (!mongoose.Types.ObjectId.isValid(id)) {
        res.status(400).json({
            message: "id is invalid!",
        })
    }
    else if (!Number.isInteger(body.starts) || body.starts < 0) {
        res.status(400).json({
            message: "starts is invalid!",
        })
    }
    else {
        let review = {
            starts: body.starts,
        };
        reviewModel.findByIdAndUpdate(id, review, (error, data) => {
            if (error) {
                res.status(500).json({
                    message: `Internal server error: ${error.message}`,
                });
            }
            else {
                res.status(200).json({
                    data
                });
            }
        })
    }
};

const deleteReviewById = (req, res) => {
    let id = req.params.reviewId;
    if (!mongoose.Types.ObjectId.isValid(id)) {
        res.status(400).json({
            message: "id is invalid!",
        })
    }
    else {
        reviewModel.findByIdAndDelete(id, (error, data) => {
            if (error) {
                res.status(500).json({
                    message: `Internal server error: ${error.message}`,
                });
            }
            else {
                res.status(204).json({
                    data
                });
            }
        })
    }
};
// export các hàm
module.exports = { getAllReviewOfCourse, getReviewById, createReviewOfCourse, updateReviewById, deleteReviewById };

