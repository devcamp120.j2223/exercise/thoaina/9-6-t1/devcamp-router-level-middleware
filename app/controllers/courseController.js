// import course model
const { mongoose } = require("mongoose");
const courseModel = require("../models/courseModel");

// get all courses
const getAllCourse = (req, res) => {
    courseModel.find((error, data) => {
        if (error) {
            res.status(500).json({
                message: `Internal server error: ${error.message}`,
            });
        }
        else {
            res.status(200).json({
                ...data
            });
        }
    })
};

const getCourseById = (req, res) => {
    let id = req.params.courseId;

    if (!mongoose.Types.ObjectId.isValid(id)) {
        res.status(400).json({
            message: "id is invalid!",
        })
    }
    else {
        courseModel.findById(id, (error, data) => {
            if (error) {
                res.status(500).json({
                    message: `Internal server error: ${error.message}`,
                });
            }
            else {
                res.status(200).json({
                    data
                });
            }
        })
    }
};

const createCourse = (req, res) => {
    // B1: thu thập dữ liệu
    let body = req.body;

    // B2: Kiểm tra dữ liệu
    if (!body.title) {
        res.status(400).json({
            message: "title is require!",
        })
    }
    else if (!Number.isInteger(body.noStudent) || body.noStudent < 0) {
        res.status(400).json({
            message: "noStudent is invalid!",
        })
    }
    else {
        // B3: Thực hiện thao tác nghiệp vụ
        let course = {
            _id: mongoose.Types.ObjectId(),
            title: body.title,
            description: body.description,
            noStudent: body.noStudent
        };
        courseModel.create(course, (error, data) => {
            if (error) {
                res.status(500).json({
                    message: `Internal server error: ${error.message}`,
                });
            }
            else {
                res.status(201).json({
                    data
                });
            }
        });
    }
};

const updateCourseById = (req, res) => {
    let id = req.params.courseId;
    let body = req.body;

    if (!mongoose.Types.ObjectId.isValid(id)) {
        res.status(400).json({
            message: "id is invalid!",
        })
    }
    else if (!body.title) {
        res.status(400).json({
            message: "title is require!",
        })
    }
    else if (!Number.isInteger(body.noStudent) || body.noStudent < 0) {
        res.status(400).json({
            message: "noStudent is invalid!",
        })
    }
    else {
        let course = {
            title: body.title,
            description: body.description,
            noStudent: body.noStudent
        };
        courseModel.findByIdAndUpdate(id, course, (error, data) => {
            if (error) {
                res.status(500).json({
                    message: `Internal server error: ${error.message}`,
                });
            }
            else {
                res.status(200).json({
                    data
                });
            }
        })
    }
};

const deleteCourseById = (req, res) => {
    let id = req.params.courseId;
    if (!mongoose.Types.ObjectId.isValid(id)) {
        res.status(400).json({
            message: "id is invalid!",
        })
    }
    else {
        courseModel.findByIdAndDelete(id, (error, data) => {
            if (error) {
                res.status(500).json({
                    message: `Internal server error: ${error.message}`,
                });
            }
            else {
                res.status(204).json({
                    data
                });
            }
        })
    }
};
// export các hàm
module.exports = { getAllCourse, getCourseById, createCourse, updateCourseById, deleteCourseById };
