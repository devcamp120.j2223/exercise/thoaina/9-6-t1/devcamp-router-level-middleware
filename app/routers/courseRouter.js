const express = require("express");
const courseMiddleware = require("../middlewares/courseMiddleware");
const courseRouter = express.Router();

courseRouter.use(courseMiddleware);

// import các hàm module controller
const { getAllCourse, getCourseById, createCourse, updateCourseById, deleteCourseById } = require("../controllers/courseController");

courseRouter.get("/courses", getAllCourse);

courseRouter.get("/courses/:courseId", getCourseById);

courseRouter.put("/courses/:courseId", updateCourseById);

courseRouter.post("/courses", createCourse);

courseRouter.delete("/courses/:courseId", deleteCourseById);

module.exports = courseRouter;