// Khai báo
const mongoose = require("mongoose");

// Khai báo thư viện Schema
const Schema = mongoose.Schema;

// Tạo đối tượng Schema tương ứng với Collection
const courseSchema = new Schema({
    _id: mongoose.Types.ObjectId,
    title: {
        type: String,
        require: true,
        unique: true,
    },
    description: {
        type: String,
        required: false,
    },
    noStudent: {
        type: Number,
        required: true,
        default: 0
    },
    reviews: [
        {
            type: mongoose.Types.ObjectId,
            ref: "review"
        }
    ]
});

// export 
module.exports = mongoose.model("Course", courseSchema);